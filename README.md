# millhouse.dev

This is my personal website where I post about topics that interest me and my open source work.

Currently this is a Golang application that can be run in two modes, `Build` and `Serve`. `Build` will build out a static website, where `Serve` will render the web pages as HTTP requests come in.

I've tried to make this application clean, fast and simple, with as few dependencies as possible (I also put it together in my free time in a couple of days so don't look too closely at the code quality). This is mainly around the lack of time to maintain this website, and any free time I have on it I'd rather spend on writing blog posts or adding functionality rather than updating old broken dependencies.

You can find the previous iteration of this website [here](https://github.com/strongishllama/millhouse.dev-frontend) and [here](https://github.com/strongishllama/millhouse.dev-cdk) 😁
