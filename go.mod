module gitlab.com/strongishllama/millhouse.dev

go 1.21.1

require (
	github.com/alecthomas/chroma v0.10.0
	github.com/spf13/pflag v1.0.5
)

require github.com/dlclark/regexp2 v1.4.0 // indirect
